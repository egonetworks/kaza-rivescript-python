#!/usr/bin/python
import os
import sys

from wsgiref.simple_server import make_server
from cgi import parse_qs, escape

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))

from flask import Flask, request, Response, jsonify
import json
from rivescript import RiveScript

virtenv = os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/'
virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
try:
    execfile(virtualenv, dict(__file__=virtualenv))
except IOError:
    pass
#
# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#

# Set up the RiveScript bot. This loads the replies from `/eg/brain` of the
# git repository.
print "preparing brain"
bot = RiveScript(debug=True)
bot.load_directory( os.path.join(os.path.dirname(__file__), ".", "eg/brain"))
bot.sort_replies()

print "brain ready"

def application(environ, start_response):

    ctype = 'application/json'
    status = '200 OK'

    if environ['PATH_INFO'] == '/reply':
      """Fetch a reply from RiveScript.
      Parameters (JSON):
      * username
      * message
      * vars
      """


      # the environment variable CONTENT_LENGTH may be empty or missing
      try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
      except (ValueError):
        request_body_size = 0

      # When the method is POST the variable will be sent
      # in the HTTP request body which is passed by the WSGI server
      # in the file like wsgi.input environment variable.
      request_body = json.loads(environ['wsgi.input'].read(request_body_size))
      # d = parse_qs(request_body)

      print request_body
      print "---"
      # print d

      '''params = request.json
      if not params:
          return jsonify({
              "status": "error",
              "error": "Request must be of the application/json type!",
          })
      '''
      username = request_body["username"]
      message  = request_body["message"]
      uservars = request_body["vars"]

      print "username:\t" + username
      print "message:\t" + message


      if uservars is None:
        uservars = dict()

      # Make sure the required params are present.
      if username is None or message is None:
          status = '400'
          response_body = jsonify({
              "status": "error",
              "error": "username and message are required keys",
          })

      # Copy and user vars from the post into RiveScript.
      if type(uservars) is dict:
          for key, value in uservars.items():
              bot.set_uservar(username, key, value)

      # Get a reply from the bot.
      reply = bot.reply(username, message)
      print "reply:\t" + reply
      # Get all the user's vars back out of the bot to include in the response.
      uservars = json.dumps(bot.get_uservars(username))
      print "uservars:\t" + uservars

      # response_body = jsonify({
      #   "status": "ok",
      #   "reply": str(reply)
      # })
      ctype = 'text/html'
      response_body = str(reply)

    elif environ['PATH_INFO'] == '/env':
        response_body = ['%s: %s' % (key, value)
                    for key, value in sorted(environ.items())]
        response_body = '\n'.join(response_body)
    else:
        ctype = 'text/html'
        response_body = "Usage: curl -i -H 'Content-Type: application/json' -X POST -d '{}' http://localhost:5000/reply"


    response_headers = [('Content-Type', ctype), ('Content-Length', str(len(response_body)))]
    #
    start_response(status, response_headers)
    return [response_body]



#
# Below for testing only
#
if __name__ == '__main__':

    httpd = make_server('localhost', 8051, application)
    # Wait for a single request, serve it and quit.
    httpd.handle_request()
