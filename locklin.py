#!/usr/bin/env python

from rivescript import RiveScript


rs = RiveScript(debug=True)
rs.load_directory("./eg/brain")
rs.sort_replies()

print """Wassup I'm Locklin, I can assist you with strong passwords
------------------------------------------------------------------------------

"""

while True:
    msg = raw_input("You> ")
    if msg == '/quit':
        quit()
    reply = rs.reply("localuser", msg)
    print "Locklin>", reply

# vim:expandtab
